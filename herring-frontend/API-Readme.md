# Herring API 
​
- This is a description of the Herring API with example responses
- currently used at [tuomiokirjat.narc.fi](https://tuomiokirjat.narc.fi)
- BASE_URL = https://transkribus.eu/r/zQPlDb1ZvVWJuiBU/api/v0
- Offset and Limit params can be set to determine how many results should be retrieved
- E.g. set offset 0 and limit 500 to retrieve the first 500 results
​
## Get list of items
​
### Request
​
`GET /items?offset=0&limit=100`
​
    curl -i -H 'Accept: application/json' https://transkribus.eu/r/zQPlDb1ZvVWJuiBU/api/v0/items?offset=0&limit=100
​
### Response
​
    {"params":{"path":[],"offset":"0","limit":"10"},"total":69,"items":["Ahvenanmaan tuomiokunnan renovoidut tuomiokirjat","Ala-Satakunnan tuomiokunnan renovoidut tuomiokirjat","Alavuden tuomiokunnan renovoidut tuomiokirjat","Etelä-Pohjanmaan tuomiokunnan renovoidut tuomiokirjat","Heinolan tuomiokunnan renovoidut tuomiokirjat","Hollolan tuomiokunnan renovoidut tuomiokirjat","Hollolan tuomiokunnan renovoidut tuomiokirjat (Hämeen lääni)","Hollolan tuomiokunnan renovoidut tuomiokirjat (Kymenkartanon lääni)","Iisalmen tuomiokunnan renovoidut tuomiokirjat","Iitin tuomiokunnan renovoidut tuomiokirjat"]}
​
## Retrive items with path
​
### Request
​
`GET /items?path[]=PATH&offset=0&limit=100`
​
    curl 'https://transkribus.eu/r/zQPlDb1ZvVWJuiBU/api/v0/items?path[]=Ahvenanmaan+tuomiokunnan+renovoidut+tuomiokirjat&offset=0&limit=100'
​
### Response example for PATH = Ahvenanmaan tuomiokunnan renovoidut tuomiokirjat
​
    {"params":{"path":["Ahvenanmaan tuomiokunnan renovoidut tuomiokirjat"],"offset":"0","limit":"72"},"total":1,"items":["Ilmoitusasioiden pöytäkirjat"]}
​
## Get nested path
​
### Request
​
`GET /items?path[]=PATH&path[]=NESTED_PATH&offset=0&limit=100'`
​
    curl 'https://transkribus.eu/r/zQPlDb1ZvVWJuiBU/api/v0/items?path[]=Ahvenanmaan+tuomiokunnan+renovoidut+tuomiokirjat&path[]=Ilmoitusasioiden+p%C3%B6yt%C3%A4kirjat&offset=0&limit=100'
### Response
​
    {"params":{"path":["Ahvenanmaan tuomiokunnan renovoidut tuomiokirjat","Ilmoitusasioiden pöytäkirjat"],"offset":"0","limit":"100"},"total":12,"items":["1827-1829","1830-1833","1834-1839","1840-1842","1843-1846","1847-1850","1851-1855","1856-1858","1859-1861","1862-1865","1866-1868","1869-1870"]}
​
## Get documents for year-range
​
### Request
​
`GET /items?path[]=PATH&path[]=NESTED_PATH&path[]=YEAR_RANGE&offset=0&limit=100`
​
    curl 'https://transkribus.eu/r/zQPlDb1ZvVWJuiBU/api/v0/items?path[]=Ahvenanmaan+tuomiokunnan+renovoidut+tuomiokirjat&path[]=Ilmoitusasioiden+p%C3%B6yt%C3%A4kirjat&path[]=1827-1829&offset=0&limit=100'
​
### Response
​
    {"total":908,"items":[{"image":{"id":"VZJHVIRKGSUBZOOQCJCEFIJQ","width":2404,"height":3606},"content":{"id":"XFPDJBKOFULIEYIBRZMVPBUS"},"meta":{"pageNum":1,"documentId":196029,"pageId":8034716}},{"image":{"id":"XSPIFWWTEXPSVWOETAXPZLFR","width":2406,"height":3634},"content":{"id":"HBJLZNZQAFURYOJBZBCCDPEG"},"meta":{"pageNum":2,"documentId":196029,"pageId":8034721}},{"image":{"id":"SKUZQXFXWSVLWKSPHKVYUVIM","width":2684,"height":4428},"content":{"id":"VRZECUALLFBLGQJKBLUQTWGM"},"meta":{"pageNum":3,"documentId":196029,"pageId":8034723}},{"image":{"id":"DREFTXRSQOSLNGYXVYDDLZQF","width":2684,"height":4462},"content":{"id":"OKCLHJXKWKAGHYCYUVOVJNOT"},"meta":{"pageNum":4,"documentId":196029,"pageId":8034728}},{"image":{"id":"LDSDNUBIAVUKQNRFPORPNVKV","width":2690,"height":4384},"content":{"id":"NUPYVDQPVSWIOPECASLZYSSF"},"meta":{"pageNum":5,"documentId":196029,"pageId":8034733}},{"image":{"id":"TOUGLPRDUSTCUEAWGZJRORYV","width":2688,"height":4422},"content":{"id":"QOELDTASATOYHIPBAHPIGJUQ"},"meta":{"pageNum":6,"documentId":196029,"pageId":8034738}}........
​
## Get image for document
​
Use image.id in previous response to retrieve image
​
### Request 
​
small : `GET https://files.transkribus.eu/iiif/2/IMAGE_ID/full/480,/0/default.jpg`
medium: `GET https://files.transkribus.eu/iiif/2/IMAGE_ID/full/720,/0/default.jpg`
large: `GET https://files.transkribus.eu/iiif/2/IMAGE_ID/full/960,/0/default.jpg`
extra-large : `GET https://files.transkribus.eu/iiif/2/IMAGE_ID/full/full/0/default.jpg`
​
    curl 'https://files.transkribus.eu/iiif/2/VZJHVIRKGSUBZOOQCJCEFIJQ/full/480,/0/default.jpg' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Connection: keep-alive'
​
### Response
​
    Image
​
## Get Page-Content as JSON
​
Use content.id in previous response to retrieve page content as JSON
​
### Request 
​
`GET /content/CONTENT_ID`
​
    curl 'https://transkribus.eu/r/zQPlDb1ZvVWJuiBU/api/v0/content/VRZECUALLFBLGQJKBLUQTWGM'
​
### Response
​
    {"declaration":{"attributes":{"version":"1.0","encoding":"UTF-8"}},"PcGts":{"type":"PcGts","attributes":{"xmlns":"http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance","xsi:schemaLocation":"http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15 http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15/pagecontent.xsd"},"Page":{"type":"Page","attributes":{"imageFilename":"IMG_31119263.jpg","imageHeight":"4428","imageWidth":"2684"},"elements":[{"type":"TextRegion","attributes":{"custom":[{"name":"readingOrder","attributes":{"index":0}}],"id":"r1","orientation":"0.0"},"geometry":{"coords":[{"x":269,"y":382},{"x":269,"y":4332},{"x":2587,"y":4332},{"x":2587,"y":382}]},"elements":[{"type":"TextLine","attributes":{"custom":[{"name":"readingOrder","attributes":{"index":0}}],"id":"r1l1"}....
​
## Get Page-Content as XML
​
### Request
​
`GET https://files.transkribus.eu/Get?id=CONTENT_ID`
​
    curl 'https://files.transkribus.eu/Get?id=VRZECUALLFBLGQJKBLUQTWGM'
​
### Response
​
  `<?xml version="1.0" encoding="UTF-8"?>
<PcGts xmlns="http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15 http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15/pagecontent.xsd">
    <Metadata>
        <Creator>prov=University of Rostock/Institute of Mathematics/CITlab/Gundram Leifert/gundram.leifert@uni-rostock.de:name=13691(htr_id=-1)::::v=2.3.1
prov=University of Rostock/Institute of Mathematics/CITlab/Tobias Gruening/tobias.gruening@uni-rostock.de:name=de.uros.citlab.module.baseline2polygon.B2PSeamMultiOriented:v=2.3.1
prov=University of Rostock/Institute of Mathematics/CITlab/Tobias Gruening/tobias.gruening@uni-rostock.de:name=/net_tf/LA73_249_0mod360.pb:de.uros.citlab.segmentation.CITlab_LA_ML:v=2.3.1
TRP</Creator>
        <Created>2019-06-29T20:45:18.083+02:00</Created>
        <LastChange>2019-06-29T20:45:37.534+02:00</LastChange>
    </Metadata>
    <Page imageFilename="IMG_31119263.jpg">....`
​
​
## Get text file
​
### Request
​
`GET /text-content/CONTENT_ID`
​
     curl 'https://transkribus.eu/r/zQPlDb1ZvVWJuiBU/api/v0/content-text/HQHWGOQTOQLQQOVSTENMYPQP'
​
### Response
​
   {"text":"Protokoll öfver lagfarts ären‐\r\nder, fordt vid lagtima härads\r\nvinter tinget med Storkyro\r\noch Ylistaro socknars tings‐\r\nlag af Ilmola domsaga, å\r\nMullo hemman i Ikola by,\r\nnedannämnde dagar År 1865.\r\nDen 8de Februari.\r\nS: 1.\r\nI egenskap af ombud för Bonden Esaias\r\nMattsson Ivar och dennes hustru Greta Isaks‐\r\ndotter, anhöll Herr Kronolänsmannen\r\nGuvernementssekreteraren Matthias Lilje\r\nqvist om första uppbudet å tjugutre nit‐\r\ntiosjettedels /15/96./ mantal af Svar skatte\r\nhemman No 1. i Topparla by, samt inlem‐\r\nnade till vinnande deraf en sålydande\r\nafhandling:\r\nHärmedelst och i kraft af detta gåfvo‐\r\nbref uppdraga och gifva vi Matts Esaiasson\r\noch Maria Jakobsdotter Svar hälften af\r\nvårt ägande 5/78 dels mantal af Ivar skatte\r\nhemman No 10. i gamla och No 1. i sednaste\r\nupprättad jordabok i Topparla by och Stor‐\r\nkyro socken eller den andel vi i ägo be‐\r\nkommit genom afhandling af den 15.\r\nFebruari 1850. utgörande tjugutre nittio‐\r\nsjettedels mantal och uppskattadt i värde\r\ntill Femhundra /500./ rubel silfver, till vår\r\nson Esaias Mattsson och dennes hustru\r\nGreta Isaksdotter, emot följande vilkor\r\nnemligen:\r\n1o Hafver köparen till oss utbetalt Två‐\r\n\r\n1\r\nta uppb: å 1/6 dels\r\nmantal af Tvar\r\nkatte hemman\r\nNo 1. i Topparla by\r\nBnn Esaias Matts‐\r\nson Svar till sä‐\r\nkerhet","id":"HQHWGOQTOQLQQOVSTENMYPQP"}
Ausblen
