### This folder contains translation files for the TranskribusLite web-interface (transkribus.eu/lite)

- To add a language to the interface or edit an existing one, please create a pull-request or contact us directly at info@readcoop.eu
- each language file is in JSON format and maps the english version of a text in the interface to it's translated counterpart
- use `en.json` as basis for a new translation
- all language files are named by the corresponding destination language, e.g. `en.json` -> English, `es.json` -> Spanish, `de.json` -> German etc. (use 2-letter code for the language as in this list: https://www.sitepoint.com/iso-2-letter-language-codes)
- `en.json` will be updated as the interface evolves -> language files have to updated too
- if a translation cannot be found in the corresponding language file, the english original is used
